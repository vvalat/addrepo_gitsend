<h1><a id="addrepo__gitsend_0"></a>addrepo &amp; gitsend</h1>
<ul>
<li>Code : Vincent Valat</li>
<li>Cadre : Coding Academy (Epitech)</li>
<li>Licence (CC) : Vous pouvez partager et adapter le code en respectant le principe d’attribution, de non-utilisation commerciale et de partage dans les mêmes conditions (<a href="https://creativecommons.org/licenses/by-nc-sa/3.0/fr/">https://creativecommons.org/licenses/by-nc-sa/3.0/fr/</a>)</li>
</ul>
<h1><a id="_1"></a></h1>
<pre><code>Installation

- décompresser la tarball
- se placer dans le dossier décompressé
=&gt; make
=&gt; make install (en mode su)

Autres options : make re, make clean
</code></pre>
<hr>
<h1><a id="_2"></a></h1>
<h2><a id="Principes_et_syntaxe_19"></a>Principes et syntaxe</h2>
<h3><a id="_3"></a>1) addrepo</h3>
<pre><code>addrepo &lt;nom du dépôt à créer&gt;
</code></pre>
<p>Effectue à travers la commande “blih” la création sécurisée du dépôt dont le nom est renseigné par l’utilisateur puis donne les droits d’accès en lecture ou écriture à un nombre variable d’utilisateurs en fonction du paramétrage initial du script. Le mot de passe n’est demandé qu’une fois, quel que soit le nombre d’opérations requises.</p>
<p>Enfin le dépôt est cloné au sein d’un répertoire racine renseigné par l’utilisateur lors du paramétrage initial du script. Le nom du répertoire est, par défaut, celui du dépôt mais peut être renommé ultérieurement via la commande “mv”.</p>
<h3><a id="_4"></a>2) gitsend</h3>
<pre><code>gitsend &lt;fichier à envoyer / expression / paramètre&gt; &quot;&lt;message de commit&gt;&quot;
</code></pre>
<p>Effectue à travers la commande “git” un “git add”, suivi d’un “git commit”, conclu par un “git push origin master”.</p>
<p>Le premier argument peut être le nom d’un fichier, une expression contenant un chemin ou le paramètre --all.</p>
<pre><code>Exemples:
gitsend --all &quot;Envoi de toutes les modifications à ce jour&quot;
gitsend /project/webform/. &quot;Dernières modifications relatives au formulaire web&quot;
gitsend fougere.c &quot;Ajout de la génération des feuilles par algorithme de fractales&quot;
</code></pre>
<h1><a id="_5"></a></h1>
<h2><a id="_6"></a>Dépannage</h2>
<p>-&gt; rien n’apparaît comme choix dans la sélection de la version de Python lors de l’installation</p>
<blockquote>
<p>Python n’a pas été détecté sur votre configuration. Vous pouvez utiliser votre gestionnaire de paquet pour installer/réinstaller une version à jour de Python ou bien créer un lien symbolique dans /usr/bin vers le bon exécutable si ce dernier se trouve à un autre emplacement. Vous devez disposer des droits d’administrateur pour effectuer cette opération.</p>
</blockquote>
<p>-&gt; gitsend / addrepo : Command not found</p>
<blockquote>
<p>Vérifiez si /usr/bin/ est bien inclus dans la variable PATH en tapant ‘echo $PATH’. Le cas échéant, tapez ‘export PATH=$PATH:/usr/bin’</p>
</blockquote>
<p>-&gt; permission denied lors de l’installation</p>
<blockquote>
<p>Le ‘make’ ne requiert pas de privilèges particuliers si vous travaillez dans vos dossiers personnels. Le cas échéant, et dans tous les cas pour le ‘make install’, vous devez disposer des droits d’administrateur pour effectuer cette opération.</p>
</blockquote>
<h1><a id="_7"></a></h1>
<h2><a id="Historique_59"></a>Historique</h2>
<p>[0.1.0] Version initiale</p>
<h1><a id="_8"></a></h1>
<h2><a id="_9"></a>Remerciements et crédits</h2>
<h1><a id="_65"></a></h1>
<p>Aalx aka notoriousPig, pour les conseils avisés et les suggestions d’adaptations nécessaires</p>
<p>Logan VanCuren, pour son script de renseignement du mot de passe (<a href="http://stackoverflow.com/a/24600839">http://stackoverflow.com/a/24600839</a>)</p>